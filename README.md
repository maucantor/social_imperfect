# The {social_imperfect} Repository: Imperfect detection of individual animals can produce imperfect descriptions of social patterns


------------------------------------------------
#### Authors
Fabio Daura-Jorge, Mauricio Cantor

#### Maintainer and contact: 
Mauricio Cantor

Department for the Ecology of Animal Societies, Max Planck Institute of Animal Behavior, Germany.

Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Brazil

Email: mcantor*ab.mpg.de



##### Version 0.0 

March 31 2021

**Abstract**

Describing social patterns in free-ranging animals requires identifying individuals and tracking their social relationships reliably. However, for marine animals, such as cetaceans and sharks, detecting and identifying individuals can be challenging. Most studies addressing the social structures of marine megafauna rely on inherently imperfect identification methods (e.g., photo-identification, acoustic tags), and disregard that individual detection probability can be sensible to individual traits. When one or more individuals of a dyad are missed, however, the common proxies of social relationship (e.g., association indices) can become imprecise, and thus misrepresent emergent social patterns. Here, we build upon recent approaches to couple mark-recapture with social network methods and control for most biases introduced by imperfect detection. First, we simulated animal populations varying in size, social connectivity and clustering, in which individuals vary in detection probabilities according to phenotypical and behavioural traits. By mapping their social networks using both a classical association metric (the Simple Ratio Index) and an adjusted index that accounts for individual detection probabilities, we show that correcting association metrics for individual detectability change both local (social preferences) and large-scale social patterns (social communities). Second, we empirically illustrate our proposed method with data from a bottlenose dolphin population from southern Brazil, where individuals were monitored through photo-identification over 12 years. We fitted mark-recapture models to these data using a set of individual covariates to estimate individual detection probabilities and correct their association indices. The classic index suggested borderline social preferences, low variability in individual centrality, moderate social differentiation, and clear large-scale social communities, whereas the adjusted index revealed more distinct preferences, more variable centrality, higher differentiation, although less obvious communities. Our study reinforces that accounting for variation in individual detection probabilities can influence our conclusions about emergent social patterns. Thus, accounting for individual detectability can be necessary, especially in small and socially sparse populations, for drawing reliable inferences on of the implications of sociality for individual lives and population processes.



#### REFERENCE

This is a supplementary material of the following article submitted to Frontiers in Marine Science.


```
Daura-Jorge, FG & Cantor M. Imperfect detection of individual animals can produce imperfect descriptions of social patterns.

```

If you use the code, data or functions, please cite the article above. If you use the empirical data provided, please also cite the following article: 

```
Daura-Jorge, F. G., Cantor, M., Ingram, S. N., Lusseau, D., & Simões-Lopes, P. C. (2012). 
The structure of a bottlenose dolphin society is coupled to a unique foraging cooperation 
with artisanal fishermen. Biology Letters, 8(5), 702-705.DOI: 10.1098/rsbl.2012.0174

```

Thank you!