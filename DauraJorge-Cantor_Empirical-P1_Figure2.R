# -----------------------------------
# Imperfect detection of individual animals can produce imperfect description of social patterns
# Fabio Daura-Jorge, Mauricio Cantor
# March 31 2021
#
# Code to estimate capture probabilities and correct association indices to investigate
# the effects of heterogeneous and imperfect detection on description of
# social patterns of wild bottlenose dolphins in Laguna, southern Brazil
# -----------------------------------



# 0. FIGURE 2: LOAD EMPIRICAL DATA AND REPLOT -----

## Obs: TO SIMPLY REPLOT (PARTS OF) FIGURE 2, WE PROVIDED OUR EMPIRICAL DATASET FOR PERIOD 1 ##
## THESE DATA COME FROM Daura-Jorge et al. 2012 Biology Letters

load("./data/DauraJorge-Cantor_Empirical-P1_Figure2.RData")

if(!require(gridExtra)){install.packages('gridExtra'); library(gridExtra)}
grid.arrange(plot.net.index.p1, plot.net.index.p1.p,
             sd_bothsuffle, strength_box, S_bothsuffle, net_bothsuffle,
             ncol=2)


## TO RUN THESE ANALYSES RELATIVE TO FIGURE 2 FROM SCRATCH, WE PROVIDED THE FOLLOWING CODE  ##

## WE PROVIDE THE DATA FOR THE SAMPLING PERIOD 1, WHICH COMES FROM Daura-Jorge et al. 2012  ##
# Daura-Jorge, F. G., Cantor, M., Ingram, S. N., Lusseau, D., & Simões-Lopes, P. C. (2012). ##
# The structure of a bottlenose dolphin society is coupled to a unique foraging cooperation ##
# with artisanal fishermen. Biology Letters, 8(5), 702-705.DOI: 10.1098/rsbl.2012.0174      ##

## THE CODE RUNS EQUALLY FOR THE EMPIRICAL DOLPHIN DATA OF SAMPLING PERIODS 2, 3  BUT THESE ##
## DATA ARE CURRENTLY EMBARGOED. THEY COULD BE EVENTUALLY AVAILABLE UPON REASONABLE REQUEST ##





## 1. LOADING --------------------------------------------------------------

if(!require(tidyverse)){install.packages('tidyverse'); library(tidyverse)}
if(!require(asnipe)){install.packages('asnipe'); library(asnipe)}
if(!require(RMark)){install.packages('RMark'); library(RMark)}
if(!require(igraph)){install.packages('igraph'); library(igraph)}
if(!require(gridExtra)){install.packages('gridExtra'); library(gridExtra)}
if(!require(sna)){install.packages('sna'); library(sna)}
if(!require(GGally)){install.packages('GGally'); library(GGally)}



# set ggplot2 theme
theme_imagej <- 
  theme(
    panel.border = element_rect(fill = 'NA'),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text = element_text(colour = "black", size = 10),
    axis.title.y = element_text(size = 10, angle = 90),
    plot.margin = margin(c(4,4,4,4))
  )



# Null model functions

#' Transforms edgelist into matrix
#'
#'@param edgelist 3-column matrix with node in, node out, weight of interaction
#'@param type \code{binary} returns a binary matrix and \code{weighted} returns a weighted matrix 
#'@author Mauricio Cantor
#'@export
el_mat <- function(edgelist, type='weighted'){
  
  edgelist=as.matrix(edgelist)
  mat=matrix(0, max(edgelist[,1]),max(edgelist[,2]))
  mat.w=mat
  
  for (k in 1:nrow(edgelist)) {
    aux1=edgelist[k,1]
    aux2=edgelist[k,2]
    mat[aux1,aux2]=1 #binary
    mat.w[aux1,aux2]=edgelist[k,3] #weighted
  }
  
  if(type=="binary"){
    return(mat)
  } else {
    return(mat.w)
  }
}



#' Transforms matrix into edgelist
#'
#'@param mat matrix of interactions
#'@param link \code{binary} returns a binary edgelist and \code{weighted} returns a weighted edgelist
#'@param type \code{"directed", "undirected"}: "directed" returns only the lower triangle of the matrix; "undirected" returns everything.
#'@param nodelabel \code{TRUE, FALSE}: "TRUE": when contains non-numerical node labels. The function will remove them and save. The output will be a list, with one object being the edgelist as a matrix and the other a vector with the new numerical labels corresponding to the original node labels
#'@author Mauricio Cantor 

mat_el <- function(mat,
                   link='weighted',
                   type='undirected',
                   nodelabel=FALSE){
  
  # makes sure it is a matrix
  mat = as.matrix(mat)
  # makes sure diagonal will be zeroed (and then removed below)
  diag(mat) = 0
  
  if(nodelabel==TRUE){
    # save node labels
    labels = rownames(mat)
    # and remove them
    rownames(mat) = colnames(mat) = NULL
  }
  
  # if the edgelist is directed, zero lower tri (and then remove below)
  if(type=="directed"){ mat[lower.tri(mat)]=0}
  
  # reshape to edgelist
  el <- reshape2::melt(mat)
  # removes zeroed interactions (including diagonal)
  el <- el[which(el[,3]!=0),]
  # removes colum names
  colnames(el) = rownames(el) <- NULL
  
  if(link=="binary"){el <- el[,1:2]}
  
  if(nodelabel==TRUE){
    result<-list()
    result[[1]] <- as.matrix(el)
    result[[2]] <- data.frame(original=labels, new=c(1:nrow(mat)))
    names(result) <- c("edgelist", "nodelabel")
    return(result)
    
  } else {
    return(el)
  }
}



#' Null model: permuting links and/or link weights among nodes
#'
#' @description Permutation of links among nodes, or link weights among nodes in a edgelist describing a network
#' @param networkdata network data in either a symetric adjacency matrix format (n-by-n matrix, where n is the number of nodes) or an edgelist (3-column matrix object, where ther first 2 columns are the nodes and the third is the link weights)
#' @param binary_or_weighted Default is \code{weighted} for inputting a weighted network; choose \code{binary} if it is a binary network --and note that the only shuffling \code{option} type that will make sence is \code{option = 'links'}
#' @param nodelabel If \code{networkdata} is a n-by-n matrix, this Boolean parameter identify if the nodes are labels: "FALSE": the nodes will be a continuous numerical ID "TRUE": when contains non-numerical node labels. The function will remove them and save. 
#' @param iter Number of random edgelists to be created
#' @param shuffleoption What to permute? \code{links}, \code{weights}, or \code{both} ?
#' @param outputformat Should the output be random edge lists (\code{edgelist}) or matrices (\code{matrix})?
#' @details Shuffles the nodes, or the link weights among nodes, or both, in a network
#' @return A set of Randomized networks in the edgelist format. If \code{nodelabel == FALSE}, the output will be a list of random edgelists; if \code{nodelabel == TRUE}, the output will be a list of a lists, with one object being the list of random edgelists, and the other a data frame with the new numerical labels corresponding to the original node labels
#' @references "Opsahl et al 2008 Physical review letters 101, 168702"; "Croft et al. 2011"; Whitehead 2008
#' @author Mauricio Cantor

nullmodel_shuffle <- function(networkdata,
                              shuffleoption='weights',
                              iter=1000,
                              binary_or_weighted='weighted',
                              outputformat='matrix',
                              nodelabel=FALSE){
  
  # force data into matrix format
  if(is.matrix(networkdata) == FALSE){ networkdata = as.matrix(networkdata)}
  
  # Check if the input is a matrix, if so then transform it into undirected edgelist
  if(nrow(networkdata) == ncol(networkdata)) {
    matel <- mat_el(networkdata,
                    link=binary_or_weighted,
                    # to return only the lower triangle, avoiding redundant links
                    type='directed', 
                    nodelabel=nodelabel)
  } else {
    # just check if the edgelist is a 3-by-n object and force it into a matrix
    if(ncol(networkdata) == 3) {
      matel = as.matrix(networkdata)
    } else {
      stop('wrong input data format. Check if it is a 3-by-n edgelist')
    }
  }
  
  # check if there are node labels, and get the appropriate object
  if(nodelabel) {
    ids = matel[[2]]
    matel = matel[[1]]
  }
  
  
  # storage for the random data
  el <- list()
  
  # If shuffling links and weights
  if(shuffleoption == 'both'){
    for(i in 1: iter){
      rand <- matel
      # shuffling nodes
      rand[,c(1,2)] <- sample(matel[,c(1,2)], replace=F)
      # shuffling link weights
      rand[,3] <- sample(matel[,3])
      doubling <- rbind(as.matrix(rand), as.matrix(rand[,c(2,1,3)]))
      rownames(doubling) <- NULL
      el[[i]] <- doubling
    }
    
    # shuffling links or weights
  } else {
    if(shuffleoption == 'links'){ shuffle = c(1,2) }
    if(shuffleoption == 'weights'){ shuffle = 3 }
    for(i in 1: iter){
      rand <- matel
      # shuffling
      rand[,shuffle] <- sample(matel[,shuffle], replace=F)
      doubling <- rbind(as.matrix(rand), as.matrix(rand[,c(2,1,3)]))
      rownames(doubling) <- NULL
      el[[i]] <- doubling
    }
  }
  
  # output format: matrix or edgelist?
  if(outputformat == 'matrix') {
    el = lapply(el, FUN = el_mat, type=binary_or_weighted) 
  }
  
  # check if there are node labels, and get the appropriate object
  if(nodelabel == TRUE) { print(ids) }
  
  # output
  return(el)
}



# SRI function ----
#'
#'@param matr group by individual matrix
#'@author Mauricio Cantor
#'
SRI =  function(matr){
  
  if (any(is.na(matr))) {
    matr <- na.omit(matr)
    cat("The data matrix contains NA, and have been removed.\n")
  }
  
  matr1 = matr
  N <- nrow(matr1)
  matr1[matr1 > 1] <- 1
  n <- apply(matr1, 2, sum)
  tmatr <- t(matr1)
  df <- as.matrix(t(matr))
  a <- df %*% t(df)
  b <- df %*% (1 - t(df))
  c <- (1 - df) %*% t(df)
  d <- ncol(df) - a - b - c
  
  Dice <- data.frame()
  inmat <- data.frame()
  denmat <- data.frame()
  
  for (i in 1:nrow(a)) {
    for (j in 1:ncol(a)) {
      # Simple Ratio Index
      Dice[i, j] <- 1 * a[i, j]/(1 * a[i, j] + b[i, j] + c[i, j])
      
      # Numerator of the SRI
      inmat[i, j] <- 1 * a[i, j] 
      
      # Denominator of the SRI
      denmat[i, j] <- (1 * a[i, j] + b[i, j] + c[i, j])
    }
  }
  
  rownames(Dice)=colnames(Dice)=colnames(matr)
  rownames(inmat)=colnames(inmat)=colnames(matr)
  rownames(denmat)=colnames(denmat)=colnames(matr)
  
  # Returns a list of 3 levels
  list(SRI = Dice, # Simple-Ratio indices
       SRI.numerator = inmat,  # Numerator of the SRI
       SRI.denominator = denmat) # Denominator of the SRI
}




# Adjusted SRI function ----
#
#' Simple Association Index Adjusted for individual capturability
#' @param matr SRI association matrix
#' @param psp.matrix.together matrix of adjusted probability of seen together
#' @param ps.matrix.separate matrix of adjusted probability of seen separate
#' @return list of three matrices of the same dimension of the input matrix (matr): SRI
#' @author Fabio Daura Jorge

SRI.adj = function(matr,
                   psp.matrix.together,
                   ps.matrix.separate){
  
  if (any(is.na(matr))) {
    matr <- na.omit(matr)
    cat("The data matrix contains NA, and have been removed.\n")
  }
  
  matr = as.matrix(matr)
  
  ap <- (matr %*% t(matr))/psp.matrix.together # seen together
  bp <- matr %*% (1 - t(matr))/diag(ps.matrix.separate) # seen separetely ya
  cp <- (1 - matr) %*% t(matr)/diag(ps.matrix.separate) # seen separetely yb
  dp <- ncol(matr) - ap - bp - cp # not seen
  
  index.p <- data.frame()
  inmat.p <- data.frame()
  denmat.p <- data.frame()
  
  for (i in 1:nrow(ap)) {
    for (j in 1:ncol(ap)) {
      # Adjusted simple Ratio Index
      index.p[i, j] <- ap[i, j]/(ap[i, j] + bp[i, j] + cp[i, j])
      # Numerator of the SRI
      inmat.p[i, j] <- ap1[i, j]
      # Denominator of the SRI
      denmat.p[i, j] <- (ap[i, j] + bp[i, j] + cp[i, j])
    }
  }
  
  # rename individuals
  labels = colnames(matr)
  rownames(index.p)=colnames(index.p)=
    rownames(inmat.p)=colnames(inmat.p)=
    rownames(denmat.p)=colnames(denmat.p) = labels
  
  # Returns a list of 3 levels
  return(list(SRI.p = index.p, # Simple-Ratio indices
              SRI.p.numerator = inmat.p,  # Numerator of the SRI adjusted
              SRI.p.denominator = denmat.p)) # Denominator of the SRI adjusted)
}



## 2. DATA INPUT -----------------------------------------------

## Obs: TO ILUSTRATE OUR APPROACH, WE PROVIDED THE DATASET FOR PERIOD 1 
load("./data/DauraJorge-Cantor_Empirical-P1_Figure2.RData")



## 3. Running closed models to find detection probabilities by sex, CN and mark -----------------------------------------------------------
## Obs.: To follow the code without running RMark, jump to line 465 and load the dataframe 'covar.p1.final'


### 3.1 The mark-recapture matrix - p1 (2007-2009) ----
dolphin.p1.df <- data.frame(
  ch = vector()
)
for(i in 1:dim(hc.matrix.p1.c)[1]){
  dolphin.p1.df[i,1]<- paste(hc.matrix.p1.c[i,1:47], collapse = "") # ch
  dolphin.p1.df[i,2] <- hc.matrix.p1.c[i,48] # fp
  dolphin.p1.df[i,3] <- hc.matrix.p1.c[i,49] # CN
  dolphin.p1.df[i,4] <- hc.matrix.p1.c[i,50] # sex
  dolphin.p1.df[i,5] <- hc.matrix.p1.c[i,51] # mark
  dolphin.p1.df[i,6] <- hc.matrix.p1.c[i,52] # id
}

dolphin.p1.df <- dolphin.p1.df %>%  # This is the final dataframe to input in the model
  dplyr::rename(fp = V2,
                CN = V3,
                sex = V4,
                mark = V5,
                id = V6) %>%
  dplyr::mutate(CN = as.factor(CN),
                sex = as.factor(sex),
                mark = as.factor(mark)) 

str(dolphin.p1.df)
summary(dolphin.p1.df)


### 3.2 Running the model p ~ beh + sex + mark --------
closed.p1.pr <- process.data(data = dolphin.p1.df,
                             model = "Closed",
                             groups= c("CN","sex","mark"))
closed.p1.ddl=make.design.data(closed.p1.pr)

run.closed.p1.id <- function() {
  p.dot = list(formula =  ~ 1, share = TRUE)
  p.beh = list(formula =  ~ CN, share = TRUE)
  p.sex = list(formula =  ~ sex, share = TRUE)
  p.mark = list(formula =  ~ mark, share = TRUE)
  p.beh.sex.mark = list(formula =  ~ CN+sex+mark, share = TRUE)
  cml <- create.model.list("Closed")
  results <- mark.wrapper(cml, data=closed.p1.pr, ddl= closed.p1.ddl)
}

closed.id.results.p1 <- run.closed.p1.id()
closed.id.results.p1$model.table <- model.table(closed.id.results.p1)


# Getting P values from model p ~ beh + sex + mark
p1.p.beh.sex.mark <- get.real(closed.id.results.p1$p.beh.sex.mark, "p", beta = NULL, se = FALSE, design = NULL,
                              data = NULL, vcv = FALSE, show.fixed = TRUE, expand = FALSE,
                              pim = TRUE)


### 3.3 Getting combined probabilities estimated by model p ~ beh + sex + mark ---------
p1.Coop.Female.Unmark <- p1.p.beh.sex.mark[1,2]
p1.Ncoop.Female.Unmark <- p1.p.beh.sex.mark[2,2]
#p1.Coop.Male.Unmark <- p1.p.beh.sex.mark[.,.] # there is no a coop.male.unmark
p1.Ncoop.Male.Unmark <- p1.p.beh.sex.mark[3,2]
p1.Coop.Female.Mark <- p1.p.beh.sex.mark[4,2]
p1.Ncoop.Female.Mark <- p1.p.beh.sex.mark[5,2]
p1.Coop.Male.Mark <- p1.p.beh.sex.mark[6,2]
p1.Ncoop.Male.Mark <- p1.p.beh.sex.mark[7,2]



## 4. Accounting for imperfect detection (ps) in association index -----------------------------------------------------------

# preparing group-by-individual (GBI)
dados.p1$IDs <- as.character(dados.p1$IDs)
dados.p1$id.list <- lapply(dados.p1$IDs, strsplit, split = " ")
dados.p1$Group <- as.factor(dados.p1$Group)
dados.p1.gbi <- lapply(dados.p1$id.list, unlist)


# Group-by-individual matrix
rawGBI.p1 <- get_group_by_individual(dados.p1.gbi, data_format = "groups")
rownames(rawGBI.p1) <- paste("G", 1:nrow(rawGBI.p1), sep = "")


# Behavioural data for p1
rawINFO.p1 <- dados.p1 %>% 
  dplyr::select(Behaviour, Interaction, Date, Group)
rownames(rawINFO.p1) <- paste("G", 1:nrow(rawGBI.p1), sep = "")


# Full set unfiltered data
GBI_fullSET_unfilt.p1 <- cbind(rawGBI.p1, rawINFO.p1)


# GBI: unfiltered, all behaviour
GBI.data.p1 <- GBI_fullSET_unfilt.p1 %>% 
  dplyr::select(-Date, -Behaviour, -Interaction, -Group)


# Order by id to match with probabilities data
GBI.data.p1 <- GBI.data.p1[order(colnames(GBI.data.p1))]


### 4.1 Calculating the classic  SRI index for p1 ------ 
# Another option is to use function SRI()

matr1 = GBI.data.p1
N1 <- nrow(matr1)
(matr1[matr1 > 1] <- 1)
n1 <- apply(matr1, 2, sum)
tmatr1 <- t(matr1)
df1 <- as.matrix(t(matr1))
a1 <- df1 %*% t(df1) # seen together - the magic matrix 
b1 <- df1 %*% (1 - t(df1)) 
c1 <- (1 - df1) %*% t(df1) 
d1 <- ncol(df1) - a1 - b1 - c1 
index.p1 <- data.frame()
denmat.p1 <- data.frame()
for (i in 1:nrow(a1)) {
  for (j in 1:ncol(a1)) {
    index.p1[i, j] <- a1[i, j]/(a1[i, j] + b1[i, j] + c1[i, j])
    denmat.p1[i, j] <- (a1[i, j] + b1[i, j] + c1[i, j])
  }
}
dimnames(index.p1) <- list(colnames(matr1), colnames(matr1))


### 4.2 Data frame with id probabilities --------
covar.p1 <- hc.matrix.p1.c[,c(49:52)]
covar.p1$ps = with(covar.p1, ifelse(covar.p1[,1] == "C" & covar.p1[,2] == "M" & covar.p1[,3] == "n", p1.Coop.Female.Unmark, 
                                    ifelse(covar.p1[,1] == "N" & covar.p1[,2] == "F" & covar.p1[,3] == "n", p1.Ncoop.Female.Unmark,
                                           ifelse(covar.p1[,1] == "N" & covar.p1[,2] == "M" & covar.p1[,3] == "n", p1.Ncoop.Male.Unmark, 
                                                  ifelse(covar.p1[,1] == "C" & covar.p1[,2] == "F" & covar.p1[,3] == "y", p1.Coop.Female.Mark,
                                                         ifelse(covar.p1[,1] == "N" & covar.p1[,2] == "F" & covar.p1[,3] == "y", p1.Ncoop.Female.Mark,         
                                                                ifelse(covar.p1[,1] == "C" & covar.p1[,2] == "M" & covar.p1[,3] == "y", p1.Coop.Male.Mark, p1.Ncoop.Male.Mark)))))))

covar.p1$id <- paste(covar.p1$id, 'L', sep='')
covar.p1.dif <- covar.p1[!(covar.p1$id %in% colnames(matr1)),]
covar.p1$id <- as.character(covar.p1$id)
covar.p1.dif$id <- as.character(covar.p1.dif$id)
covar.p1.final <- anti_join(covar.p1,covar.p1.dif)


### ** If not running RMark, then retrive the necessary data frame 'covar.p1.final' ** ###
covar.p1.final


# Vector with individual probabilities
pro.p1<-covar.p1.final$ps 


# Diagonal matrix with individual probabilities
ps.mat.p1 <- diag(pro.p1) 


# To create the product of probabilities per dyad
psp.mat.p1 <- diag(pro.p1)


# Names
colnames(ps.mat.p1) <- covar.p1.final$id
rownames(ps.mat.p1) <- covar.p1.final$id
colnames(psp.mat.p1) <- covar.p1.final$id
rownames(psp.mat.p1) <- covar.p1.final$id


### 4.3 Creating the product of probabilities per dyad -------

# Another option is to use the function multiply.probs() in our simulation code

# Manually:
psp.mat.p1[1, 2:length(n1)] <- ps.mat.p1[1, 1]*pro.p1[-1]
psp.mat.p1[2, 3:length(n1)] <- ps.mat.p1[2, 2]*pro.p1[-c(1:2)]
psp.mat.p1[3, 4:length(n1)] <- ps.mat.p1[3, 3]*pro.p1[-c(1:3)]
psp.mat.p1[4, 5:length(n1)] <- ps.mat.p1[4, 4]*pro.p1[-c(1:4)]
psp.mat.p1[5, 6:length(n1)] <- ps.mat.p1[5, 5]*pro.p1[-c(1:5)]
psp.mat.p1[6, 7:length(n1)] <- ps.mat.p1[6, 6]*pro.p1[-c(1:6)]
psp.mat.p1[7, 8:length(n1)] <- ps.mat.p1[7, 7]*pro.p1[-c(1:7)]
psp.mat.p1[8, 9:length(n1)] <- ps.mat.p1[8, 8]*pro.p1[-c(1:8)]
psp.mat.p1[9, 10:length(n1)] <- ps.mat.p1[9, 9]*pro.p1[-c(1:9)]
psp.mat.p1[10, 11:length(n1)] <- ps.mat.p1[10, 10]*pro.p1[-c(1:10)]
psp.mat.p1[11, 12:length(n1)] <- ps.mat.p1[11, 11]*pro.p1[-c(1:11)]
psp.mat.p1[12, 13:length(n1)] <- ps.mat.p1[12, 12]*pro.p1[-c(1:12)]
psp.mat.p1[13, 14:length(n1)] <- ps.mat.p1[13, 13]*pro.p1[-c(1:13)]
psp.mat.p1[14, 15:length(n1)] <- ps.mat.p1[14, 14]*pro.p1[-c(1:14)]
psp.mat.p1[15, 16:length(n1)] <- ps.mat.p1[15, 15]*pro.p1[-c(1:15)]
psp.mat.p1[16, 17:length(n1)] <- ps.mat.p1[16, 16]*pro.p1[-c(1:16)]
psp.mat.p1[17, 18:length(n1)] <- ps.mat.p1[17, 17]*pro.p1[-c(1:17)]
psp.mat.p1[18, 19:length(n1)] <- ps.mat.p1[18, 18]*pro.p1[-c(1:18)]
psp.mat.p1[19, 20:length(n1)] <- ps.mat.p1[19, 19]*pro.p1[-c(1:19)]
psp.mat.p1[20, 21:length(n1)] <- ps.mat.p1[20, 20]*pro.p1[-c(1:20)]
psp.mat.p1[21, 22:length(n1)] <- ps.mat.p1[21, 21]*pro.p1[-c(1:21)]
psp.mat.p1[22, 23:length(n1)] <- ps.mat.p1[22, 22]*pro.p1[-c(1:22)]
psp.mat.p1[23, 24:length(n1)] <- ps.mat.p1[23, 23]*pro.p1[-c(1:23)]
psp.mat.p1[24, 25:length(n1)] <- ps.mat.p1[24, 24]*pro.p1[-c(1:24)]
psp.mat.p1[25, 26:length(n1)] <- ps.mat.p1[25, 25]*pro.p1[-c(1:25)]
psp.mat.p1[26, 27:length(n1)] <- ps.mat.p1[26, 26]*pro.p1[-c(1:26)]
psp.mat.p1[27, 28:length(n1)] <- ps.mat.p1[27, 27]*pro.p1[-c(1:27)]
psp.mat.p1[28, 29:length(n1)] <- ps.mat.p1[28, 28]*pro.p1[-c(1:28)]
psp.mat.p1[29, 30:length(n1)] <- ps.mat.p1[29, 29]*pro.p1[-c(1:29)]
psp.mat.p1[30, 31:length(n1)] <- ps.mat.p1[30, 30]*pro.p1[-c(1:30)]
psp.mat.p1[31, 32:length(n1)] <- ps.mat.p1[31, 31]*pro.p1[-c(1:31)]
psp.mat.p1[32, 33:length(n1)] <- ps.mat.p1[32, 32]*pro.p1[-c(1:32)]
psp.mat.p1[33, 34:length(n1)] <- ps.mat.p1[33, 33]*pro.p1[-c(1:33)]
psp.mat.p1[34, 35:length(n1)] <- ps.mat.p1[34, 34]*pro.p1[-c(1:34)]
psp.mat.p1[35, 36:length(n1)] <- ps.mat.p1[35, 35]*pro.p1[-c(1:35)]
psp.mat.p1[36, 37:length(n1)] <- ps.mat.p1[36, 36]*pro.p1[-c(1:36)]
psp.mat.p1[37, 38:length(n1)] <- ps.mat.p1[37, 37]*pro.p1[-c(1:37)]
psp.mat.p1[38, 39:length(n1)] <- ps.mat.p1[38, 38]*pro.p1[-c(1:38)]
psp.mat.p1[39, 40:length(n1)] <- ps.mat.p1[39, 39]*pro.p1[-c(1:39)]
psp.mat.p1[40, 41:length(n1)] <- ps.mat.p1[40, 40]*pro.p1[-c(1:40)]

# Or semi-automatically:
# for (i in 1:(nrow(psp.mat.p1)-1)) {
#   psp.mat[i, (i+1):length(pro.p1) ] <- ps.mat.p1[i, i] * pro.p1[-c(1:i)]
# }

psp.mat.p1[lower.tri(psp.mat.p1)] <- t(psp.mat.p1)[lower.tri(psp.mat.p1)]



## 5. Adding detection probabilities in SRI, the SRI.adj, for p1 ---------------------------

### 5.1 The SRI.adj ------

# Another option is to use  function SRI.adj()

ap1 <- (df1 %*% t(df1))/psp.mat.p1 # seen together
bp1 <- df1 %*% (1 - t(df1))/diag(ps.mat.p1) # not seen together
cp1 <- (1 - df1) %*% t(df1)/diag(ps.mat.p1) # not seen together
dp1 <- ncol(df1) - ap1 - bp1 - cp1 # not seen
index.p1.p <- data.frame()
inmat.p1.p <- data.frame()
denmat.p1.p <- data.frame()
for (i in 1:nrow(ap1)) {
  for (j in 1:ncol(ap1)) {
    index.p1.p[i, j] <- ap1[i, j]/(ap1[i, j] + bp1[i, j] + cp1[i, j]) # adjusted SRI
    inmat.p1.p[i, j] <- ap1[i, j]  # numerator
    denmat.p1.p[i, j] <- (ap1[i, j] + bp1[i, j] + cp1[i, j]) # denominator
  }
}
dimnames(index.p1.p) <- list(colnames(matr1), colnames(matr1))


# Preparing network data

diag(index.p1) <- 0      
diag(index.p1.p) <- 0      
index.p1 <- data.matrix(index.p1)
index.p1.p <- data.matrix(index.p1.p)
ig_by_index.p1 <- graph.adjacency(index.p1, mode="undirected", diag=FALSE, weighted=TRUE)
ig_by_index.p1.p <- graph.adjacency(index.p1.p, mode="undirected", diag=FALSE, weighted=TRUE)


# Network plots 

par(mfrow=c(1,2), mar=c(0.1,0.1,1,0.1))

plot(ig_by_index.p1, edge.arrow.size=.5, vertex.size=8, 
     main=paste('SRI period 1', names(ig_by_index.p1)[i]),
     vertex.frame.color="gray", vertex.label.color="black", 
     vertex.label.cex=0.8, vertex.label.dist=2, edge.curved=0)

plot(ig_by_index.p1.p, edge.arrow.size=.5, vertex.size=8,
     main=paste('SRI period 1 adj', names(ig_by_index.p1.p)[i]),
     vertex.frame.color="gray", vertex.label.color="black", 
     vertex.label.cex=0.8, vertex.label.dist=2, edge.curved=0)


# Calculating modularity (option 1)
(modularity_by_index.p1  <- igraph::cluster_leading_eigen(ig_by_index.p1))
(modularity_by_index.p1.p  <- igraph::cluster_leading_eigen(ig_by_index.p1.p))


### 5.2 Plotting networks with modularity SRI vs SRIadj -----
par(mfrow=c(1,2), mar=c(0.1,0.1,1,0.1))

plot(ig_by_index.p1,
     vertex.size=8,
     #vertex.label="",
     vertex.frame.color="gray",
     vertex.label.color="black", vertex.label.cex=0.8, vertex.label.dist=2, edge.curved=0, 
     vertex.frame.width=0.01,
     mark.groups = modularity_by_index.p1,
     #mark.border = colfunc(2)[2:1],
     #mark.col = alpha(colfunc(2)[2:1],0.1),
     main=paste('SRI period 1', names(ig_by_index.p1)))

plot(ig_by_index.p1.p,
     vertex.size=8,
     #vertex.label="",
     vertex.frame.color="gray",
     vertex.label.color="black", vertex.label.cex=0.8, vertex.label.dist=2, edge.curved=0, 
     vertex.frame.width=0.01,
     mark.groups = modularity_by_index.p1.p,
     #mark.border = colfunc(2)[2:1],
     #mark.col = alpha(colfunc(2)[2:1],0.1),
     main=paste('SRI period 1 adj', names(ig_by_index.p1.p)))



## 6. Testing modularity significance ----

# Using the null model to take an adjacency matrix from one of the simulated scenarios, and shuffle links and weights among pairs of nodes

### 6.1 Recalculate the modularity, using walktrap algorithm ------
obs_mod.empirical = data.frame(network = c('p1','p1.adj'),
                               Q = c(modularity(walktrap.community(ig_by_index.p1)),
                                     modularity(walktrap.community(ig_by_index.p1.p))),
                               nmodule = c(length(unique(membership(walktrap.community(ig_by_index.p1)))),
                                           length(unique(membership(walktrap.community(ig_by_index.p1.p))))))

obs_mod.empirical$lowCI_bothsuffle = NA
obs_mod.empirical$upCI_bothsuffle = NA
obs_mod.empirical$lowCI_nmodule = NA
obs_mod.empirical$upCI_nmodule = NA



# Create 1000 random networks by shuffling BOTH links among nodes and link weights, USING THE OBSERVED MATRIX AS INPUT
ig_by_index.p1.nullmat_bothshuf <- nullmodel_shuffle(
  networkdata = index.p1,
  binary_or_weighted='weighted',
  nodelabel=TRUE,
  shuffleoption='both',
  iter=1000,
  outputformat='matrix')

ig_by_index.p1.p.nullmat_bothshuf <- nullmodel_shuffle(
  networkdata = index.p1.p,
  binary_or_weighted='weighted',
  nodelabel=TRUE,
  shuffleoption='both',
  iter=1000,
  outputformat='matrix')


# Storage
ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod = ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod = 
  matrix(NA, ncol=2, nrow=1000)

colnames(ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod) = colnames(ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod) = 
  c('Q', 'nmodule')


for(i in 1:1000){
  d.p1_bothshuf <- graph.adjacency(as.matrix(ig_by_index.p1.nullmat_bothshuf[[i]]), mode="undirected", diag=FALSE, weighted = T)
  d.p1.p_bothshuf <- graph.adjacency(as.matrix(ig_by_index.p1.p.nullmat_bothshuf[[i]]), mode="undirected", diag=FALSE, weighted = T)
  
  aux13 = walktrap.community(d.p1_bothshuf)
  ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod[i,1] = modularity(aux13) 
  ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod[i,2] = length(unique(membership(aux13)))
  
  aux14 = walktrap.community(d.p1.p_bothshuf)
  ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod[i,1] = modularity(aux14)
  ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod[i,2] = length(unique(membership(aux14)))
  
}


# Calculate 95% CI of modularity for random networks
obs_mod.empirical[1,4:5] = 
  quantile(ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod[,1], probs = c(0.025, 0.975), type=2)

obs_mod.empirical[1,6:7] = 
  quantile(ig_by_index.p1.edgelist.unst_nullmat_bothshuf.randmod[,2], probs = c(0.025, 0.975), type=2)

obs_mod.empirical[2,4:5] = 
  quantile(ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod[,1], probs = c(0.025, 0.975), type=2)

obs_mod.empirical[2,6:7] = 
  quantile(ig_by_index.p1.p.edgelist.unst_nullmat_bothshuf.randmod[,2], probs = c(0.025, 0.975), type=2)


# Cleaning
rm(aux13, aux14, d1)



### 6.2 Plotting modularity results -----

obs_mod.empirical$period = rep(c('P1'), each=2)
obs_mod.empirical$index = rep(c('SRI', 'SRI.adj'), each=1)
colperiods = c('#C03221')

(net_bothsuffle <- obs_mod.empirical %>%
    ggplot(aes(x = network, y=Q, group=1, color = period, fill=period)) +
    geom_errorbar(width=.2, aes(ymin=lowCI_bothsuffle, ymax=upCI_bothsuffle)) +
    geom_point(aes(shape=index), size=4, alpha=.8) +
    scale_color_manual(values = colperiods) +
    scale_fill_manual(values = colperiods) +
    scale_y_continuous('Q (95%CI)',
                       limits = c(0, (max(obs_mod.empirical$Q))+0.05)) +
    scale_x_discrete('Network (original vs. adjusted)') +
    theme_imagej +
    ggtitle("Empirical modularity") +
    theme(legend.position='none',
          legend.title = element_blank()))




## 7. Testing preferred/avoided associations ------

# Using the SD and/or CV of the associations as a metric to test for preferred/avoided associations. 
# The null model is that individuals associate at random; if so, then the variance of the observed associations (here taken as the SD or the CV of the observed SRI and the SRI.adjust matrices) would be higher than expected by chance. 
# The rationale is that more variable association indices indicate that some pairs of individuals associate more often ('preferred') or less often ('avoided') than expected if individuals associate randomly.

### 7.1 Calculate the SD and the CV of the associations -------

sd.mat = function(mat){
  mat = as.matrix(mat)
  return(sd(mat[lower.tri(mat, diag=F)]))
}


obs_var.empirical = data.frame(network = c('p1','p1.adj'),
                               SD = c(sd.mat(index.p1), sd.mat(index.p1.p)))


# storage for random values
obs_var.empirical$lowCI_SD = NA
obs_var.empirical$upCI_SD = NA


# Retrieve the 1000 random networks created by shuffling BOTH links among nodes and link weigths, USING THE OBSERVED MATRIX AS INPUT
ig_by_index.p1.bothshuf.sdcv = ig_by_index.p1.p.bothshuf.sdcv =
  matrix(NA, ncol=1, nrow=1000)
colnames(ig_by_index.p1.bothshuf.sdcv) = colnames(ig_by_index.p1.p.bothshuf.sdcv) = 
  c('SD')


# calculate SD of randomized networks
for(i in 1:1000){
  # p1
  ig_by_index.p1.bothshuf.sdcv[i,1] = sd.mat(ig_by_index.p1.nullmat_bothshuf[[i]])
  # p1.p
  ig_by_index.p1.p.bothshuf.sdcv[i,1] = sd.mat(ig_by_index.p1.p.nullmat_bothshuf[[i]])
}


# Calculate 95% CI of SD and CV for random networks
obs_var.empirical[1,3:4] = quantile(ig_by_index.p1.bothshuf.sdcv[,1],  probs = c(0.025, 0.975), type=2)
obs_var.empirical[2,3:4] = quantile(ig_by_index.p1.p.bothshuf.sdcv[,1],probs = c(0.025, 0.975), type=2)


### 7.2 Plot SD --------

obs_var.empirical$period = rep(c('P1'), each=2)
obs_var.empirical$index = rep(c('SRI', 'SRI.adj'), each=1)
colperiods = c('#C03221')


(sd_bothsuffle <- obs_var.empirical %>%
  ggplot(aes(x = network, y=SD, group=1, color = period, fill=period)) +
  geom_errorbar(width=.2, aes(ymin=lowCI_SD, ymax=upCI_SD)) +
  geom_point(aes(shape=index), size=4, alpha=.8) +
  scale_color_manual(values = colperiods) +
  scale_fill_manual(values = colperiods) +
  scale_y_continuous('SD association (95%CI)',
                     limits = c(max(0, min(obs_var.empirical[, c(2,3,4)]) - 0.02),
                                max(obs_var.empirical[, c(2,3,4)]) + 0.02),
                     breaks = round(seq(max( min(obs_var.empirical[, c(2,3,4)]) - 0.02, 0),
                                        max(obs_var.empirical[, c(2,3,4)]) + 0.02, by=0.02), 2)) +
  scale_x_discrete('Network (original vs. adjusted)') +
  theme_imagej +
  ggtitle("Empirical variation in associations") +
  theme(legend.position='none',
        legend.title = element_blank()))



## 8. Strength ----


### 8.1 Calculate the weighted degree per individual -----

stp1 = data.frame(strength = igraph::strength(ig_by_index.p1, mode='all', loops=F))
stp1$network = 'p1'
stp1$period = 'P1'
stp1p = data.frame(strength = igraph::strength(ig_by_index.p1.p, mode='all', loops=F))
stp1p$network = 'p1.adj'
stp1p$period = 'P1'
obs_strength.empirical = rbind(stp1, stp1p)


### 8.2 Plot weighted degree ------
(strength_box = obs_strength.empirical %>% 
  mutate(index = factor(network, levels=c('p1','p1.adj'),
                        labels = c('SRI','SRI.adj'))) %>% 
  ggplot(aes(y = (strength), x = network, color = period, fill=period)) +
  geom_boxplot(outlier.colour = NA, alpha=0.2) +
  geom_jitter(aes(shape=index), size=3, width = 0.15, alpha=0.5) +
  scale_color_manual(values = colperiods) +
  scale_fill_manual(values = colperiods) +
  scale_y_continuous('Strenght (Association index)')+
  scale_x_discrete('Network (original vs. adjusted)') +
  theme_imagej +
  ggtitle("Empirical association strength") +
  theme(legend.position='none',
        legend.title = element_blank()))



## 9. Comparing Social differentiation ------


### 9.1 Calculating S based on Mike Weiss github package 
source('https://raw.githubusercontent.com/MNWeiss/aninet/master/R/social_differentiation.R')
source('https://raw.githubusercontent.com/MNWeiss/aninet/master/R/convenience_functions.R')


# Retrieve the GBI matrices
GBI.data.p1 


# Calculate S for the observed data
# calculate numerator and denominator of association indices, SRI
p1.numerator <- get_numerator(as.matrix(GBI.data.p1), return = "vector", data_format = "GBI")
p1.denominator <- get_denominator(as.matrix(GBI.data.p1), return = "vector", data_format = "GBI")


# Calculate Social differentiation, S, per period 
p1.S = social_differentiation(Num = p1.numerator,
                              Den = p1.denominator,
                              method = "Whitehead", #"Beta-binomial", 
                              res = 0.001,  initial.params = c(0.1,0.1))


# Calculating S for the adjusted data
# retrieving the numerator and denominator of the adusted SRI.p
inmat.p1.p
denmat.p1.p


# transforming these into vectors to match the get_numerator() and get_denominator outputs
p1p.numerator <- inmat.p1.p[lower.tri(inmat.p1.p)]

p1p.denominator <- denmat.p1.p[lower.tri(denmat.p1.p)]


# Calculate Social differentiation, S, for the adjusted data
p1p.S = social_differentiation(Num = p1p.numerator,
                               Den = p1p.denominator,
                               method = "Whitehead", #"Beta-binomial", 
                               res = 0.001,  initial.params = c(0.1,0.1))


# Organizing the results and plotting
obs_S_empirical = data.frame(network = c('p1','p1.adj'))

obs_S_empirical = cbind(obs_S_empirical, 
                        rbind(p1.S[2,], p1p.S[2,]))

obs_S_empirical$period = rep(c('P1'), each=2)
obs_S_empirical$index = rep(c('SRI', 'SRI.adj'), each=1)
obs_S_empirical

colperiods = c('#C03221')


### 9.2 Plot S ------

(S_bothsuffle <- obs_S_empirical %>%
    ggplot(aes(x = network, y=Estimate, group=1, color = period, fill=period)) +
    geom_errorbar(width=.2, aes(ymin=`Lower CI`, ymax=`Upper CI`)) +
    geom_point(aes(shape=index), size=4, alpha=.8) +
    scale_color_manual(values = colperiods) +
    scale_fill_manual(values = colperiods) +
    scale_y_continuous('S (95%CI)',
                       limits = c( min(obs_S_empirical$`Lower CI`, na.rm=T)-0.1,
                                   max(obs_S_empirical$`Upper CI`, na.rm=T) +0.1) ) +
    scale_x_discrete('Network (original vs. adjusted)') +
    theme_imagej +
    ggtitle("Empirical Social Differentiation") +
    theme(legend.position='none',
          legend.title = element_blank()))



## 10. Plot networks ------

# SRI network plot
net.index.p1 <- as.network(index.p1.p, 
                           matrix.type='adjacency',
                           directed = F,
                           ignore.eval=FALSE,
                           names.eval='weight')

plot.net.index.p1 = ggnet2(
  net.index.p1,
  color = c('black', colperiods[1])[as.numeric(factor(covar.p1.final$CN))],
  alpha = 0.9, 
  size = as.numeric(covar.p1.final$mark),
  shape = as.numeric(covar.p1.final$sex)+14,
  edge.size = get.edge.value(net.index.p1, "weight")*4,
  edge.color = 'grey25',
  edge.alpha = 0.25,
  mode = 'fruchtermanreingold',
  label= NA,
  label.size=0,
  legend.position = 0,
  legend.size = 0
) + theme(axis.line.x = element_blank(),
          axis.line.y = element_blank()) +
  ggtitle('Period 1, classic SRI')




# Adjusted SRI network plot
net.index.p1.p <- as.network(index.p1.p, 
                           matrix.type='adjacency',
                           directed = F,
                           ignore.eval=FALSE,
                           names.eval='weight')

plot.net.index.p1.p = ggnet2(
  net.index.p1.p,
  color = c('black', colperiods[1])[as.numeric(factor(covar.p1.final$CN))],
  alpha = 0.9, 
  size = as.numeric(covar.p1.final$mark),
  shape = as.numeric(covar.p1.final$sex)+14,
  edge.size = get.edge.value(net.index.p1, "weight")*4,
  edge.color = 'grey25',
  edge.alpha = 0.25,
  mode = 'fruchtermanreingold',
  label= NA,
  label.size=0,
  legend.position = 0,
  legend.size = 0
) + theme(axis.line.x = element_blank(),
          axis.line.y = element_blank()) +
  ggtitle('Period 1, adjusted SRI')






## 11. PLOT FIGURE 2: only for P1 ------


grid.arrange(plot.net.index.p1, plot.net.index.p1.p,
                  sd_bothsuffle, strength_box, S_bothsuffle, net_bothsuffle,
                  ncol=2)
